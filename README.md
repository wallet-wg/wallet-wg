# Wallet-WG

**Note to Reader**: *This document is an early-stage draft intended to spur discussion by the prospective Wallet Working Group members. The content provided below is offered as a starting point and expected to undergo revision. If this document makes a statement or takes an approach, you disagree with, or you can provide a better plan, please contribute your input. The goal is to cultivate a working group that shares ideas and reaches those ideas by way of discussion and debate. Critical feedback is crucial in achieving a good result.*

# Tezos Wallet Working Group Proposal

## Mission

To assist wallet developers in producing the best Tezos wallet experience for wallet users in the blockchain ecosystem.

The Working Group sets out to provide wallet developers with:

* A catalogue of baseline functionality expected of a Tezos wallet. Functionality is expressed in the form of a User Story and contextualized with a functional description.
* Details on Technical Considerations and known common pitfalls.
* Suggestions on UX Design patterns that are successful, and troublesome patterns.

**note**: Everything in this document is offered as a starting point. The effort is to find agreement on what to document, how to maintain these resources, and how to to keep them relevant and useful.

## How to Contribute

We are interested in having everyone participate in refining the goals of the wallet working group by contributing their ideas to the Mission, Baseline, and Upcoming sections of this current [**readme**.](https://gitlab.com/tezosagora/wallet-wg/-/blob/master/README.md)

Once you have read the current document, please open a merge request with your changes and message the wallet working group in slack to encourage discussion.

## Baseline Features

### Wallet Generation / Import

A Tezos wallet must facilitate the signing of operations. For the wallet to sign operations, it must either store the keys or interact with a device or system that can sign operations on the wallets behalf.

#### Creating new keys (hot wallet)

As a user who does not have an existing Tezos wallet or address, I want to generate a new wallet so that I can receive tokens from a third-party

##### Technical Considerations

- 160-256 bits entropy (preferably 256) based on cryptographically secure randomness. BIP39 mnemonic phrase for recovery. Enforce a high bit-strength for the password if an encrypted key store is created. For symmetric encryption, AES GCM is often a good choice. Choice KDF, parameters and salt based on the latest recommendations (NIST is a good source here).

##### Key Generation Best Practices

- Discourage use of copy to clipboard for seed words
- Verify the user has retained the seed words

##### Key Generation items for WG discussion

- Verification of seed words via ordering based selection to mitigate keylogger risk
- Determine if useful to have a consistent approach to documenting and explaining encrypted Keystore formats and usage to users
- Allow user to specify the target curve (tz1, tz2, tz3)

### Import/Restore wallet

As a user who has a private key in the form of a mnemonic phrase, I want to restore my wallet in the wallet program so that I can use my wallet to manage my Tezos tokens.

### Import/Restore Technical Considerations

It should always be possible to import with the mnemonic phrase. Validate with the built-in checksum and sanitize the mnemonic phrase.

#### Import/Restore Best Practices

#### Import/Restore For discussion

- Should the default be to require a tz1 address for proper input validation to lessen user confusion when the unexpected resulting account shows a zero balance? The user would be required to explicitly turn off the validation by effectively confirming, "I don't have my tz1 account address, which I'm restoring."
- Allow user to specify the target curve (tz1, tz2, tz3)

### Activate fundraiser wallet

Used to activate a Tezos fundraiser allocation after the KYC/AML has been completed

As a Tezos fundraiser participant who has not activated my account, I want to activate my account using a Tezos wallet so that I can use, transfer or delegate my tokens as I see fit.

#### Fundraiser Activation Technical Considerations

An anonymous operation that doesn't require any fee or a valid signature.

#### Fundraiser Activation Best Practices

#### Fundraiser Activation For discussion

- Consider reducing visibility as activation is no longer a high priority and a source of confusion. As time passes, the demand for this use case will diminish further.

### Delegate tokens to a baker

As a user who has a wallet and some tokens, I want to delegate my tokens so that I can participate in the staking rewards that are part of the Tezos blockchain

#### Delegation Technical Considerations

- (Todo) Should describe how wallet devs can reliably source a list of bakers for delegation

#### Delegation Best Practices

- Display a choice of delegates from an unbiased source rather than a paid or limited list
- No default delegate, the default should either be 'custom', or it should be randomly selected from the list
- (Recommended) Wallet should validate and either disallow/warn users when they try to delegate to a baker that won't pay them for non-custodial delegation (e.g. we see people delegating to Coinbase, Binance)

#### TBD Delegation topics for discussion

- It is possible to undelegate, but having this feature too visible often creates confusion for the users. They might get the impression that they first need to undelegate before they set a new delegate. Tezos uses LPoS, and undelegating is not a beneficial action under typical use cases. If a wallet wants to support undelegation, it might be a good idea to let delegation to an empty string result in an undelegation.
- Warn user in case the baker is `dead` (not paying rewards), `over-delegated` (thus will not pay rewards), or has an extremely high fee
- Allow multi-delegation

### Sending Tezos tokens

As a Tezos token holder, I want to send a portion of my tokens to an address of my choosing so that I can transfer tokens.

### Sending Tezos tokens Technical Considerations

* Operations are initially represented as a JSON object (link). If the public key of the receiving address is not revealed yet, a reveal operation must be completed. "counter" and "branch" values must be fetched from the node RPC API. Gas and storage limits can be set more accurately by first simulating the operation. Fees can be set by assuming non-full blocks and base it on what would pass the pre-validation in a default node. Another approach is to base the fee on historical data (more future proof). Perform input validation, create the JSON object, forge locally (validate against remote forge if the local forge implementation isn't tested comprehensively), sign (0x03 watermark), pre-apply and inject. Operations can be sent individually or in a batch.

#### Sending Tezos Tokens Best Practices

- Validate that destination is not a known baker and warn (we sometimes see new users confused and sending their tokens to a baker rather than delegating them)

#### Sending Tezos Tokens items for discussion

- Display the expected storage cost (burn) in addition to the fee
- Check if the destination account is allocated. If it's not, warn the user about additional costs, ensure there are sufficient funds
- Handle refused operations (stuck in "mempool" or refused by the node), 'counter-in-the-past/in-the-future' issue workaround

### Originate a manager .tz account

#### Originate manager Technical Considerations

- Scriptless contracts have been migrated to "manager.tz" contracts. This contract can be originated and used as an extra account with the same functionality as an implicit account.

#### Originate manager Best Practices

- Due to costs and user confusion about sending from a "manager.tz" account, it's better to use implicit accounts.


#### Originate manager Items for discussion

- Help user to solve the issue when he cannot withdraw funds from the manager .tz contract due to the lack of funds, suggest using Tezos faucet. Generally, this applies to all smart contract interactions, especially tokens (typical issue).

### Input validation and related error handling

#### Input validation and error handling Technical Considerations

- Many errors are prevented with input validation and use of dry run or pre-apply.

##### Input validation and error handling Best Practices

- When an error occurs, good error handling and messages, so the user understands why the action failed. Sensible error handling reduces user confusion leading to a better experience.

##### Input valiadation and error handling Items for discussion

- Can we standardize on common error messages?

#### Node/Indexer communication error handling

##### Node/Indexer Technical Considerations

_TBD_

##### Node/Indexer Best Practices

- Inform the user of node errors and guide the user to solve the problem.

##### Node/Indexer For Discussion

- Implement an auto-retry (for reads only) policy before displaying errors to the user.
- Check the node is in sync (head timestamp is up to date) and ensure `chain_id` is valid
- Implement switching to a fallback node (pool of nodes) in case of connectivity or out-of-sync issues
- Allow user to set the node address manually

### List implicit accounts and balances

#### Implicit Accounts Technical Considerations

- Implicit accounts are derived locally in the wallet and the balances fetched from the node RPC.

#### Implicit Accounts Best Practices

- Auto-refresh changes to accounts.

#### List originated accounts and balances

##### Originated Accounts Technical Considerations

- Fetching originated accounts requires an indexer.

### List sent operations

As a wallet user, I want to see what operations have been sent from my address in chronological order.

#### List sent Operations Technical Considerations

Requires an indexer to retrieve. In addition to transactions, display other operations such as activations, reveals, originations, and delegations.

#### List sent Operations Items for discussion

- Display pending operations (injected but not in chain yet)
- Display refused operations (stuck in mempool or refused by the node due to an error), show visual notification

### List received Operations

As a wallet user, I want to see a chronological list of all the operations received by my wallet/account.

#### List received Operations Technical Considerations

Requires an indexer to retrieve.

#### List received Operations Items for discussion

- (Recommended) Auto refresh changes and display a visual notification
- Push notifications

### List active delegation

As a wallet user who has delegated my tokens, I want to see what baker my tokens are delegated to so that I know which baker my tokens are delegated to.

#### List active delegation Technical Considerations

- Fetched from the node RPC.

#### List active delegation for discussion

* Display current delegation status (time-to-first-reward)

## HD Private Key Storage

HD Key Storage support by way of devices such as a Ledger device is strongly recommended.

### Ledger support for Private Key storage and signing

#### Ledger support Technical Considerations
#### Ledger support Best Practices
#### Ledger support for discussion

- Resolve or document any issues with large operations.
- Specify device models expected to be supported

### Upcoming features needing discussion for v2 of this "standard" for more advanced wallets

- Token Support
  - FA2, FA1.2, other?
- HD Wallet
  - BIP44, legacy HD paths
  - Curve-specific implementation details (especially ed25519)
  - Account discovery procedure
- Arbitrary contract calls
- RPC node failover
