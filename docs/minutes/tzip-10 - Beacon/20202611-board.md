# Tezos - Wallet Working Group - tzip-10 & Beacon Workgroup Minutes - 26.11.20

## Attendees

- Jev Björsell (JB) Taquito/ECAD Labs
- Inna Stetsiak (IS) Taquito/ECAD Labs
- Roxane Létourneau (RL) Taquito - Wallet API/ECAD Labs
- Claude Barde (CB) Taquito - dApps/ECAD Labs
- Korney Vasilchenko (KV) Thanos/Madfish Solutions
- Klas Harrysson (KH) Kukai/Tezos Commons
- Luis Gonzalez (LG) Kukai/Tezos Commons
- Andreas Gassmann (AG) Beacon/AirGap
- Pascal Brun (PB) Beacon/AirGap

## Agenda

1. Beacon SDK changes in the last week
1. Status Beacon Extension/AirGap Wallet v2 compatibility
1. Status Thanos Beacon v2 compatibility
1. Status Kukai Beacon v2 compatibility
1. Status Taquito Beacon v2 compatibility
1. Status Tezos Domains & Dexter
1. Testing status in issue https://github.com/airgap-it/beacon-sdk/issues/45
1. Release schedule & communication

## Minutes

- [AG] showcases the latest Beacon SDK changes with Beacon SDK beta v16
- [CB] mentions issue encountered with latest Taquito version and switching between various extensions that cause a problem
  - The testing approach will be documented in the issue https://github.com/airgap-it/beacon-sdk/issues/45
- Add web wallets on iOS to the list as they can be opened as well on the mobile devices
- Add web wallets on Android below the "Connect Wallet" button as they can be opened as well on mobile devices
- Discussion about handling different networks with Kukai's special case as they've deployed different versions for the networks and at the moment approach is to have the option to select the network up to the wallet.
  - Add a flag to the constructor to distinguish between mainnet/testnet
  - How to handle Tezos related test net names e.g. Delphinet in order to break existing dApps/wallets
  - → Issue
- [AG] dApp status update after reachout
  - Status of Dexter is in testing
  - Tezos Domains tested and provided feedback that the selection list should be configurable as they want to provide their own user interface
- [LG] mentioned that the Baking Bad team is working on a .Net implementation of the Beacon SDK for Atomex
- [KV] Thanos pushed their implementation to the stores and it’s in production
  - Working on implementation of Beacon v2 on Quipuswap
- [KH] Kukai should be available soon, they need the version to point to their testnet
- [RL] Taquito, some more work needs to be put into the connection handling
  - schedule individual call to resolve this
- [LG] introduction of payload type for message signing to future proofing Beacon SDK
  - potential change for message signing in a protocol upgrade with a DoNotInject sequence to prevent injection of a potential vulnerability through a signed message

## Next steps

- Monday (30.11.20) announce Beacon v2 maturity in the interaction channel
- Next Thursday check call for announcement
- Continue testing with dApp side

## Next calls

- tzip-10 & arbitrary message signing future proofing - 01.11.20
- tzip-10 & Beacon - 03.11.20
