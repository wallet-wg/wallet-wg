# Tezos Foundation

# Wallet Working Group Glossary

## Note

The terms defined herein are provided with definitions within the context
of Tezos and its Wallet Working Group.

## Terms and Definitions

[Account](#account): an accounting of XTZ funds that is identified by a
public key and controlled by a private key

[Alias](#alias): a human-readable label for a public key; usu. is local; i.e.
is not on the Blockchain

[Blockchain](#blockchain): a history of operations on a network that is formed
by consensus of the Nodes

[Certification](#certification): an official recognition that an application
meets Standards (at least at a given point in time)

[Contract](#contract): a.k.a. Smart Contract; code that defines functions which
resides within the Blockchain; it may be used to process function calls and for
tracking operations for applications

[Developers](#developers): a Member or a team or company that builds and
releases one or many Wallet(s)

[Guidelines](#guidelines): a set of recommendations--not usually considered
enforceable but that is encouraged

[Network](#network): a series of Nodes that build and maintain the blockchain

[Node](#node): a server that participates in a Tezos network

[Operation](#operation): an action that requires processing by Nodes

[Standards](#standards): a set of prescribed rules that are intended to be
adhered to by Members

[Transaction](#transaction): an operation which transfers of XTZ

[TZIP](#tzip): see [TZIP Repository](https://gitlab.com/tzip/tzip)

[Wallet](#wallet): a software application that allows the management of
accounts
